document.getElementById('task-form').addEventListener('submit', function(event) {
    event.preventDefault();

    const taskInput = document.getElementById('task-input');
    const taskList = document.getElementById('task-list');

    if (taskInput.value) {
        const newTask = document.createElement('li');
        newTask.innerHTML = `
            <input type="checkbox">
            <span>${taskInput.value}</span>
            <button class="ml-2 text-red-500"><i class="fas fa-trash-alt"></i></button>
        `;
        newTask.classList.add('my-2', 'flex', 'items-center');

        newTask.querySelector('button').addEventListener('click', function() {
            taskList.removeChild(newTask);
        });

        newTask.querySelector('input').addEventListener('change', function() {
            newTask.querySelector('span').classList.toggle('line-through', this.checked);
        });

        taskList.appendChild(newTask);
        taskInput.value = '';
    }
});
